package stepDefinition;

import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.firefox.FirefoxDriver; 
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestStepDefinition {
	@Given("print some thing")
	public void print_some_thing() throws Exception{ 
	 System.out.println("Hello");
	}
	@When("I open browser")
	public void i_open_browser() throws Exception{
		 System.out.println("Open browser"); 
		// Initialize browser
		 System.setProperty("webdriver.chrome.driver", "driver\\chromedriver.exe");
		 WebDriver driver = new ChromeDriver();
		// WebDriver driver = new FirefoxDriver(); 
		  
		 // Open Google
		 driver.get("http://www.google.com");
		  
		 // Close browser
		// driver.close();
	}

	@Then("browser should open")
	public void browser_should_open() throws Exception{
		 System.out.println("Sucess");  
	
	}


}
